package com.example.teamweatherapp;

import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyAsyncClass extends AsyncTask<    String, String, JSONObject>{
    private Activity mainActivity;

    public MyAsyncClass(Activity activity) {
        // 呼び出し元のアクティビティ
        this.mainActivity = activity;
    }
    // 非同期処理
    @Override
    protected JSONObject doInBackground(String ...managers) {
        JSONObject json = null;
        try {
            // Sleep処理（例：HTTP通信）
            Thread.sleep(1000);
            while (true) {
                json = WeatherInfoManager.getInstance().getWeatherInfo(mainActivity);
                if (json != null){
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return json ;
    }
    @Override
    protected void onProgressUpdate(String...params) {
        String[] data = {"Please wait"};
        TextView t1 = mainActivity.findViewById(R.id.detail);
        TextView detail = mainActivity.findViewById(R.id.displaydetail);
        t1.setText("Now loading");
        detail.setText("Please wait");
    }
    // 非同期処理が終了後、結果をメインスレッドに返す
    @Override
    protected void onPostExecute(JSONObject result) {

        TextView t1 = mainActivity.findViewById(R.id.detail);
        TextView detail = mainActivity.findViewById(R.id.displaydetail);
        String label = "";
        try{
            JSONObject description = result.getJSONObject("description");
            detail.setText(description.getString("text"));
           JSONArray description2 = result.getJSONArray("forecasts");
           Log.d("activity",description2.toString());
           for (int i = 0;i < description2.length();i++){
               JSONObject data = description2.getJSONObject(i);
               label += data.getString("dateLabel");
               label += ":";
               label += data.getString("telop");
               label += "\n";
           }
            t1.setText(label);
        }catch(JSONException | NullPointerException e){
            Log.d("MainActivity",e.toString());
            Toast.makeText(mainActivity,e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
