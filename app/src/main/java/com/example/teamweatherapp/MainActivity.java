package com.example.teamweatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private MyAsyncClass testTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeTextView();
    }

    public void Onclick(View view){
        Intent i = new Intent(this,AnotherActivity.class);
        startActivity(i);
    }
    public void changeTextView() {
        // 非同期処理(AsyncHttpRequest#doInBackground())を呼び出す
        new MyAsyncClass(this).execute("");
    }
}
