package com.example.teamweatherapp;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class WeatherInfoManager
{
    private RequestQueue requestQueue;
    private JSONObject weatherInfo;
    private final String URL = "http://weather.livedoor.com/forecast/webservice/json/v1?city=130010";

    private static final WeatherInfoManager ourInstance = new WeatherInfoManager();

    public static WeatherInfoManager getInstance() {
        return ourInstance;
    }

    private WeatherInfoManager() {
    }

    public JSONObject getWeatherInfo(final Context context) {
        if(weatherInfo == null) {
            if(requestQueue == null) {
                requestQueue = Volley.newRequestQueue(context);
            }

            // Write your code here
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.GET, URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            weatherInfo = response;
                            Log.d("Mainactivity",response.toString());
                        }
                        },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error){
                            Log.d("Mainactivity",error.toString());
                        }
                    }
            );
            requestQueue.add(request);
        }

        return weatherInfo;
    }


}
