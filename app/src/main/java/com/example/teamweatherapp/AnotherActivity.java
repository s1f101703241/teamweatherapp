package com.example.teamweatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AnotherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);
        ListView listview = findViewById(R.id.displaylocation);
        WeatherInfoManager manager = WeatherInfoManager.getInstance();
        JSONObject json = manager.getWeatherInfo(AnotherActivity.this);
        ArrayList<String> name_array = new ArrayList<String>();
        final ArrayList<String> link_array = new ArrayList<String>();
        try {
            JSONArray nameinfo = json.getJSONArray("pinpointLocations");
            for (int i = 0;i < nameinfo.length();i++){
                JSONObject data = nameinfo.getJSONObject(i);
                String name = data.getString("name");
                String link = data.getString("link");
                name_array.add(name);
                link_array.add(link);
            }
            listview.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,name_array));
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String url = link_array.get(position);
                            Uri uri = Uri.parse(url);
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            // Verify that the intent will resolve to an activity
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                // Here we use an intent without a Chooser unlike the next example
                                startActivity(intent);
                            }
                        }
                    });
        }catch (JSONException | NullPointerException e){
            Toast.makeText(AnotherActivity.this,e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
